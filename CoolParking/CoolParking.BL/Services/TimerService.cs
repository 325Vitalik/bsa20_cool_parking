﻿// TODO: implement class TimerService from the ITimerService interface.
//       Service have to be just wrapper on System Timers.

using CoolParking.BL.Interfaces;
using System;
using System.Timers;

namespace CoolParking.BL.Services
{
    public class TimerService : ITimerService
    {
        private Timer timer;

        public double Interval { get; set; }

        public event ElapsedEventHandler Elapsed;

        public TimerService(double intervalInSeconds)
        {
            this.Interval = intervalInSeconds*1000;
            this.timer = new Timer(Interval);
            this.timer.Elapsed += invokeElapsedEvent;

            this.timer.AutoReset = true;
            this.timer.Enabled = true;
        }

        public void Dispose()
        {
            this.timer.Dispose();
        }

        public void Start()
        {
            this.timer.Start();
        }

        public void Stop()
        {
            this.timer.Stop();
        }

        private void invokeElapsedEvent(Object source, ElapsedEventArgs e)
        {
            if (this.Elapsed == null) return;
            this.Elapsed.Invoke(this, null);
        }
    }
}