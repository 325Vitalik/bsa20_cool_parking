﻿// TODO: implement the LogService class from the ILogService interface.
//       One explicit requirement - for the read method, if the file is not found, an InvalidOperationException should be thrown
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in LogServiceTests you can find the necessary constructor format.

using CoolParking.BL.Interfaces;
using System.IO;

namespace CoolParking.BL.Services
{
    public class LogService : ILogService
    {
        public string LogPath {get;}

        public LogService(string filePath)
        {
            this.LogPath = filePath;
        }

        public string Read()
        {
            if (!File.Exists(LogPath))
            {
                throw new System.InvalidOperationException("File doesn't exist");
            }
            using var fileStream = new StreamReader(LogPath);

            string line;
            string result = "";

            while ((line = fileStream.ReadLine()) != null)
            {
                result += line + "\n";
            }

            return result;
        }

        public void Write(string logInfo)
        {
            if(logInfo == "")
            {
                return;
            }
            using var fileStream = new StreamWriter(LogPath, true);

            fileStream.WriteLineAsync(logInfo);
        }
    }
}