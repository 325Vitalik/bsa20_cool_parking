﻿// TODO: implement the ParkingService class from the IParkingService interface.
//       For try to add a vehicle on full parking InvalidOperationException should be thrown.
//       For try to remove vehicle with a negative balance (debt) InvalidOperationException should be thrown.
//       Other validation rules and constructor format went from tests.
//       Other implementation details are up to you, they just have to match the interface requirements
//       and tests, for example, in ParkingServiceTests you can find the necessary constructor format and validation rules.

using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using System;
using System.Collections.ObjectModel;
using System.Linq;

namespace CoolParking.BL.Services
{
    public class ParkingService : IParkingService
    {
        private Parking parking;
        private ITimerService withdrawTimer;
        private ITimerService logTimer;
        private ILogService logService;

        public ParkingService(ITimerService withdrawTimer, ITimerService logTimer, ILogService logService)
        {
            this.withdrawTimer = withdrawTimer;
            this.logTimer = logTimer;
            this.logService = logService;
            this.parking = Parking.ParkingInstance(logService);

            this.withdrawTimer.Elapsed += this.parking.Payment;
            this.logTimer.Elapsed += this.parking.WriteLogs;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if(this.parking.FreePlaces == 0)
            {
                throw new InvalidOperationException("No free places");
            }
            if(this.parking.GetAllVehicles().Count(v => v.Id == vehicle.Id) > 0)
            {
                throw new ArgumentException("Vehicle with this Id already exist");
            }

            this.parking.AddVehicle(vehicle);
        }

        public decimal GetBalance()
        {
            return this.parking.Balance;
        }

        public int GetCapacity()
        {
            return Settings.ParkingCapacity;
        }

        public int GetFreePlaces()
        {
            return this.parking.FreePlaces;
        }

        public TransactionInfo[] GetLastParkingTransactions()
        {
            return this.parking.GetLastTransactions();
        }

        public ReadOnlyCollection<Vehicle> GetVehicles()
        {
            return this.parking.GetAllVehicles();
        }

        public string ReadFromLog()
        {
            return this.logService.Read();
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = this.parking.GetAllVehicles().FirstOrDefault(v => v.Id == vehicleId);
            if (vehicle == null)
            {
                throw new ArgumentException("Vehicle with this Id doesn't exist");
            }
            if(vehicle.Balance < 0)
            {
                throw new InvalidOperationException("Balance of this vehicle is negative");
            }
            this.parking.RemoveVehicle(vehicleId);
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            if (this.parking.GetAllVehicles().FirstOrDefault(v => v.Id == vehicleId) == null)
            {
                throw new ArgumentException("Vehicle with this Id doesn't exist");
            }
            if(sum < 0)
            {
                throw new ArgumentException("Sum is negative");
            }
            this.parking.TopUpVehicle(vehicleId, sum);
        }

        public void Dispose()
        {
            this.parking.Dispose();
            this.logTimer.Dispose();
            this.withdrawTimer.Dispose();
        }
    }
}
