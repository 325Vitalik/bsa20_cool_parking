﻿// TODO: implement class Vehicle.
//       Properties: Id (string), VehicleType (VehicleType), Balance (decimal).
//       The format of the identifier is explained in the description of the home task.
//       Id and VehicleType should not be able for changing.
//       The Balance should be able to change only in the CoolParking.BL project.
//       The type of constructor is shown in the tests and the constructor should have a validation, which also is clear from the tests.
//       Static method GenerateRandomRegistrationPlateNumber should return a randomly generated unique identifier.

using System;
using System.Text.RegularExpressions;

namespace CoolParking.BL.Models
{
    public class Vehicle
    {
        public string Id { get; }
        public VehicleType VehicleType { get; }
        public decimal Balance { get; set; }

        public Vehicle(string id, VehicleType vehicleType, decimal balance)
        {
            var regexValidationId = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            if (!regexValidationId.IsMatch(id))
            {
                throw new ArgumentException("Invalid Id");
            }
            else if(balance < 0)
            {
                throw new ArgumentException("Balance is negative");
            }

            this.Id = id;
            this.VehicleType = vehicleType;
            this.Balance = balance;
        }

        public static string GenerateRandomRegistrationPlateNumber()
        {
            var randomId = generateRandomSymbols('A', 'Z', 2) + "-" + generateRandomSymbols(0, 10, 4) + "-" + generateRandomSymbols('A', 'Z', 2);
            return randomId;
        }

        private static string generateRandomSymbols(int min, int max, int numberOfSymbols)
        {
            Random rnd = new Random();
            string randomStr = "";
            for (int i = 0; i < numberOfSymbols; i++)
            {
                randomStr += (char)rnd.Next(min, max);
            }
            return randomStr;
        }
    }
}