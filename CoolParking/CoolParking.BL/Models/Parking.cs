﻿// TODO: implement class Parking.
//       Implementation details are up to you, they just have to meet the requirements 
//       of the home task and be consistent with other classes and tests.

using CoolParking.BL.Interfaces;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Threading;
using System.Timers;

namespace CoolParking.BL.Models
{
    public class Parking : IDisposable
    {
        public int FreePlaces { get; private set; } = 0;
        public decimal Balance { get; private set; }

        private static Parking parkingInstance;
        readonly ILogService logService;

        private int parkingCapacity;
        private List<Vehicle> vehiclesInParking;
        private List<TransactionInfo> Transactions;

        private Parking(ILogService logService)
        {
            this.Balance = Settings.Balance;
            this.parkingCapacity = Settings.ParkingCapacity;
            this.FreePlaces = Settings.ParkingCapacity;
            this.logService = logService;

            this.vehiclesInParking = new List<Vehicle>();
            this.Transactions = new List<TransactionInfo>();
        }

        public static Parking ParkingInstance(ILogService logService)
        {
            if(parkingInstance == null)
            {
                parkingInstance = new Parking(logService);
                return parkingInstance;
            }
            return parkingInstance;
        }

        public void AddVehicle(Vehicle vehicle)
        {
            if(parkingCapacity > vehiclesInParking.Count)
            {
                this.FreePlaces--;
                vehiclesInParking.Add(vehicle);
            }
        }

        public void RemoveVehicle(string vehicleId)
        {
            var vehicle = vehiclesInParking.FirstOrDefault(v => v.Id == vehicleId);
            if(vehicle != null)
            {
                this.FreePlaces++;
                vehiclesInParking.Remove(vehicle);
            }
        }

        public void TopUpVehicle(string vehicleId, decimal sum)
        {
            vehiclesInParking = vehiclesInParking.Select(v => 
            {
                if (v.Id == vehicleId) 
                {
                    v.Balance += sum;
                }
                return v;
            }).ToList();
        }

        public ReadOnlyCollection<Vehicle> GetAllVehicles()
        {
            return this.vehiclesInParking.AsReadOnly();
        }

        public TransactionInfo[] GetLastTransactions()
        {
            return this.Transactions.ToArray();
        }

        public void WriteLogs(Object source, ElapsedEventArgs e)
        {
            this.logService.Write("");

            var transactions = this.Transactions.ToList();
            this.Transactions.Clear();

            foreach (var transactionInfo in transactions)
            {
                var logStr = transactionInfo.Time + ": "+ transactionInfo.Sum + " money withdrawn from vehicle with Id='" + transactionInfo.VehicleId+"'."; 
                this.logService.Write(logStr);
            }
        }

        public void Payment(Object source, ElapsedEventArgs e)
        {
            for(int i = 0; i < this.vehiclesInParking.Count; i++)
            {
                switch (vehiclesInParking[i].VehicleType) 
                {
                    case VehicleType.Bus:
                        getPaymentOFVehicle(vehiclesInParking[i], Settings.BusRate);
                        break;
                    case VehicleType.Motorcycle:
                        getPaymentOFVehicle(vehiclesInParking[i], Settings.MotorcycleRate);
                        break;
                    case VehicleType.PassengerCar:
                        getPaymentOFVehicle(vehiclesInParking[i], Settings.PassengerCarRate);
                        break;
                    case VehicleType.Truck:
                        getPaymentOFVehicle(vehiclesInParking[i], Settings.TruckRate);
                        break;
                    default:
                        break;
                }
            }
        }

        private void getPaymentOFVehicle(Vehicle vehicle, decimal rate)
        {
            //TODO: make readable
            var transactionInfo = new TransactionInfo();
            transactionInfo.Time = DateTime.Now;
            transactionInfo.VehicleId = vehicle.Id;

            if (vehicle.Balance > 0 && vehicle.Balance < rate)
            {
                var sumWithoutFine = vehicle.Balance;
                var sumWithFine = Math.Abs(vehicle.Balance - rate) * Settings.FineRatio;

                vehicle.Balance = -sumWithFine;

                this.Balance += sumWithFine+sumWithoutFine;

                transactionInfo.Sum = sumWithFine + sumWithoutFine;
            }
            else if (vehicle.Balance > 0)
            {
                vehicle.Balance -= rate;
                this.Balance += rate;
                transactionInfo.Sum = rate;
            }
            else
            {
                vehicle.Balance -= rate * Settings.FineRatio;
                this.Balance += rate * Settings.FineRatio;
                transactionInfo.Sum = rate * Settings.FineRatio;
            }

            this.Transactions.Add(transactionInfo);
        }

        public void Dispose()
        {
            vehiclesInParking.Clear();
            Transactions.Clear();
            parkingInstance = null;
        }
    }
}