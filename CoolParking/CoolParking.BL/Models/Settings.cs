﻿// TODO: implement class Settings.
//       Implementation details are up to you, they just have to meet the requirements of the home task.

using System;

namespace CoolParking.BL.Models
{
    public static class Settings
    {
        public static decimal Balance { get; set; } = 0;
        public static int ParkingCapacity { get; set; } = 10;
        public static int PaymentPeriod { get; set; } = 5;
        public static int WritingToLogPeriod { get; set; } = 60;
        public static decimal PassengerCarRate { get; set; } = 2;
        public static decimal TruckRate { get; set; } = 5;
        public static decimal BusRate { get; set; } = 3.5M;
        public static decimal MotorcycleRate { get; set; } = 1;
        public static decimal FineRatio { get; set; } = 2.5M;
    }
}