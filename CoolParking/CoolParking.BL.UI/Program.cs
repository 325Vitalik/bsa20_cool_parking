﻿using CoolParking.BL.Interfaces;
using CoolParking.BL.Services;
using CoolParking.BL.Models;
using System;
using System.Threading;
using System.Runtime.CompilerServices;
using System.Linq;
using System.Linq.Expressions;
using System.Text.RegularExpressions;
using System.Text;

namespace CoolParking.BL.UI
{
    class Program
    {
        private static ITimerService withdrawTimer;
        private static ITimerService logTimer;
        private static ILogService logService;
        private static ParkingService parkingService;

        static void Main(string[] args)
        {
            withdrawTimer = new TimerService(Settings.PaymentPeriod);
            logTimer = new TimerService(Settings.WritingToLogPeriod);
            logService = new LogService(@"Transactions.log");

            parkingService = new ParkingService(withdrawTimer, logTimer, logService);

            mainMenu();
        }

        private static void mainMenu()
        {
            while (true)
            {
                Console.WriteLine("1| Вивести на екран поточний баланс Паркiнгу.\n" +
                                    "2| Вивести на екран суму зароблених коштiв за поточний перiод(до запису у лог).\n" +
                                    "3| Вивести на екран кiлькість вiльних мiсць на паркуваннi(вiльно X з Y).\n" +
                                    "4| Вивести на екран усi Транзакцiї Паркiнгу за поточний перiод(до запису у лог).\n" +
                                    "5| Вивести на екран iсторiю Транзакцiй(зчитавши данi з файлу Transactions.log).\n" +
                                    "6| Вивести на екран список Тр.засобiв, що знаходяться на Паркiнгу.\n" +
                                    "7| Поставити Транспортний засiб на Паркiнг.\n" +
                                    "8| Забрати Транспортний засiб з Паркiнгу.\n" +
                                    "9| Поповнити баланс конкретного Тр.засобу.\n"+
                                    "0| Завершити роботу\n");
                int n = 0;
                Console.Write("Введiть цифру 0-9: ");
                try
                {
                    n = Int32.Parse(Console.ReadLine().Trim());
                }
                catch(FormatException e)
                {
                    Console.Clear();
                    writeErrorMessage("Введiть будь ласка число в дiапазонi 0-9");
                    continue;
                }

                Console.WriteLine();

                switch (n)
                {
                    case 0:
                        parkingService.Dispose();
                        return;
                    case 1:
                        showBalance();
                        break;
                    case 2:
                        showCurrentIncome();
                        break;
                    case 3:
                        showFreePlaces();
                        break;
                    case 4:
                        showTransactionsOfCurrentPeriod();
                        break;
                    case 5:
                        showLogs();
                        break;
                    case 6:
                        showVehiclesInParking();
                        break;
                    case 7:
                        putVehicleInParking();
                        break;
                    case 8:
                        removeVehicleFromParking();
                        break;
                    case 9:
                        topUpVehicle();
                        break;
                    default:
                        Console.Clear();
                        writeErrorMessage("Введiть будь ласка число в дiапазонi 0-9");
                        continue;
                }

                Console.Write("\nНажмiть будь-яку клавiшу щоб повернутись в головне меню: ");
                Console.ReadKey();
                Console.Clear();
            }
        }

        private static void showBalance()
        {
            var balance = parkingService.GetBalance();
            writeSuccessMessage("На балансi паркiнгу: " + balance);
        }

        private static void showCurrentIncome()
        {
            var transactions = parkingService.GetLastParkingTransactions().ToList();
            var income = transactions.Select(t => t.Sum).Sum();
            writeSuccessMessage("Прибуток за поточний перiод: " + income);
        }

        private static void showFreePlaces()
        {
            var free = parkingService.GetFreePlaces();
            var capacity = parkingService.GetCapacity();
            writeSuccessMessage($"Вiльних мiсць: {free}/{capacity}");
        }

        private static void showTransactionsOfCurrentPeriod()
        {
            var transactions = parkingService.GetLastParkingTransactions()?.ToList();
            if (transactions == null || transactions.Count == 0)
            {
                writeSuccessMessage("Наразi немає транзакцiй\n");
                return;
            }
            writeSuccessMessage("Останi транзакцiї:\n");
            foreach (var transaction in transactions)
            {
                Console.WriteLine($"{transaction.Time} sum: {transaction.Sum} from: {transaction.VehicleId}\n");
            }
        }

        private static void showLogs()
        {
            try
            {
                var logs = parkingService.ReadFromLog();
                writeSuccessMessage("Попереднi транзакцiї:");
                Console.WriteLine(logs);
            }
            catch(InvalidOperationException e)
            {
                writeErrorMessage(e.Message);
            }
        }

        private static void showVehiclesInParking()
        {
            var vehicles = parkingService.GetVehicles()?.ToList();
            if (vehicles == null || vehicles.Count == 0)
            {
                writeSuccessMessage("Тр. засобiв на паркiнгу немає\n");
                return;
            }
            writeSuccessMessage("Тр. засоби на паркiнгу:\n");
            foreach (var vehicle in vehicles)
            {
                Console.WriteLine($"{vehicle.VehicleType}\t{vehicle.Id}\t Balance: {vehicle.Balance}\n");
            }
        }

        private static void putVehicleInParking()
        {
            Vehicle vehicle;

            while (true)
            {
                VehicleType vehicleType = VehicleType.Bus;
                int userAns;

                string vehicleId = readVehicleId();
                if (vehicleId == null) return;

                decimal vehicleBalance = readBalance();
                if (vehicleBalance == -1) return;

                //read number of vehicleType
                while (true)
                {
                    try
                    {
                        Console.WriteLine("Оберiть тип Тр. засобу:\n" +
                                            "\t1. PassengerCar\n" +
                                            "\t2. Truck\n" +
                                            "\t3. Bus\n" +
                                            "\t4. Motorcycle");
                        userAns = Int32.Parse(Console.ReadLine().Trim());
                        if (userAns < 1 || userAns > 4)
                        {
                            throw new FormatException();
                        }
                        break;
                    }
                    catch (FormatException e)
                    {
                        writeErrorMessage("Введiть будь ласка число в дiапазонi 1 - 4");
                    }
                }

                switch (userAns)
                {
                    case 1:
                        vehicleType = VehicleType.PassengerCar;
                        break;
                    case 2:
                        vehicleType = VehicleType.Truck;
                        break;
                    case 3:
                        vehicleType = VehicleType.Bus;
                        break;
                    case 4:
                        vehicleType = VehicleType.Motorcycle;
                        break;
                }

                try
                {
                    vehicle = new Vehicle(vehicleId, vehicleType, vehicleBalance);
                }
                catch (ArgumentException e)
                {
                    writeErrorMessage(e.Message);

                    Console.Write("Ви хочете повернутись в головне менню (так/інше): ");
                    var ans = Console.ReadLine().Trim().ToLower();
                    if (ans == "так")
                    {
                        return;
                    }

                    continue;
                }
                break;
            }
            parkingService.AddVehicle(vehicle);

            writeSuccessMessage("Тр.засiб успiшно додано");
        }

        private static void removeVehicleFromParking()
        {
            string vehicleId = readVehicleId();
            if (vehicleId == null) return;

            try
            {
                parkingService.RemoveVehicle(vehicleId);
                writeSuccessMessage("Тр. засiб успiшно вилученно з паркiнгу");
            }
            catch (InvalidOperationException e)
            {
                writeErrorMessage("Баланс цього Тр. засобу мiнусовий, щоб забрати поповнiть баланс");
                return;
            }
            catch (ArgumentException e)
            {
                writeErrorMessage("Цього Тр. засобу немає на паркiнгу!");
                return;
            }
        }

        private static void topUpVehicle()
        {
            var vehicleId = readVehicleId();
            if (vehicleId == null) return;

            var sum = readBalance();
            if (sum == -1) return;

            try
            {
                parkingService.TopUpVehicle(vehicleId, sum);
            }
            catch(ArgumentException e)
            {
                writeErrorMessage(e.Message);
                return;
            }
            writeSuccessMessage("Баланс успiшно збiльшено!");
        }

        
        private static string readVehicleId()
        {
            string vehicleId;

            while (true)
            {
                try
                {
                    Console.Write("Введiть id(формат XY-1234-TR): ");
                    vehicleId = readLineWithCancel()?.Trim();

                    if(vehicleId == null)
                    {
                        return null;
                    }

                    var regexValidationId = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
                    if (!regexValidationId.IsMatch(vehicleId))
                    {
                        throw new ArgumentException();
                    }
                    break;
                }
                catch (ArgumentException e)
                {
                    writeErrorMessage("Id має бути формату XY-1234-TR");
                }
            }

            return vehicleId;
        }

        private static decimal readBalance()
        {
            decimal vehicleBalance;

            while (true)
            {
                try
                {
                    Console.Write("Введiть суму: ");
                    var balanceStr = readLineWithCancel()?.Trim();
                    if (balanceStr == null) return -1;

                    vehicleBalance = decimal.Parse(balanceStr);
                    if(vehicleBalance < 0)
                    {
                        throw new FormatException();
                    }
                    break;
                }
                catch (FormatException e)
                {
                    writeErrorMessage("Введiть будь ласка додатне число");
                }
            }

            return vehicleBalance;
        }

        private static string readLineWithCancel()
        {
            string result = null;

            StringBuilder buffer = new StringBuilder();

            ConsoleKeyInfo info = Console.ReadKey(true);
            int i = -1;
            while (info.Key != ConsoleKey.Enter && info.Key != ConsoleKey.Escape)
            {
                if (info.Key == ConsoleKey.Backspace && i > -1)
                {
                    buffer.Remove(i, 1);
                    Console.Write("\b");
                    Console.Write(" ");
                    Console.Write("\b");

                    i--;
                }
                else if(info.Key != ConsoleKey.Backspace)
                {
                    Console.Write(info.KeyChar);
                    buffer.Append(info.KeyChar);
                    i++;
                }
                info = Console.ReadKey(true);
            }

            if (info.Key == ConsoleKey.Enter)
            {
                Console.WriteLine("\n");
                result = buffer.ToString();
            }

            return result;
        }


        private static void writeErrorMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine(message + "\n");
            Console.ForegroundColor = ConsoleColor.White;
        }

        private static void writeSuccessMessage(string message)
        {
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine(message + "\n");
            Console.ForegroundColor = ConsoleColor.White;
        }
    }
}
