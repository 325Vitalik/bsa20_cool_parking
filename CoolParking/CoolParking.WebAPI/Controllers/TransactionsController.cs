﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Services;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/transactions")]
    [ApiController]
    public class TransactionsController : ControllerBase
    {
        private IParkingService parkingService;

        public TransactionsController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        // GET: api/transactions/last
        [Route("last")]
        [HttpGet]
        public IActionResult GetLastTransaction()
        {
            var transaction = parkingService.GetLastParkingTransactions().Select(t => TransactionVM.CreateInstance(t)).ToArray();

            var jsonString = JsonConvert.SerializeObject(transaction);

            return Ok(jsonString);
        }

        // GET api/transactions/all
        [Route("all")]
        [HttpGet]
        public IActionResult GetAllTransactions()
        {
            string transactionsString = "";
            try
            {
                transactionsString = parkingService.ReadFromLog();
            }
            catch(Exception e)
            {
                return StatusCode(404, e.Message);
            }

            return Ok(transactionsString);
        }

        // PUT api/transactions/topUpVehicle
        [Route("topUpVehicle")]
        [HttpPut]
        public IActionResult TopUpVehicle([FromBody]TopUpVM vm)
        {
            if (!Validation.CheckId(vm.Id))
            {
                return StatusCode(400, "Invalid Id");
            }

            if(vm.Sum < 0)
            {
                return StatusCode(400, "Sum should be positive");
            }

            try
            {
                parkingService.TopUpVehicle(vm.Id, vm.Sum);

                var vehicle = parkingService.GetVehicles().FirstOrDefault(v => v.Id == vm.Id);
                var jsonString = JsonConvert.SerializeObject(vehicle);

                return Ok(jsonString);
            }
            catch(Exception e)
            {
                return StatusCode(404, e.Message);
            }
        }
    }
}
