﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using CoolParking.BL.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using System.Net.Http;
using System.Net;
using System.Text.RegularExpressions;
using CoolParking.WebAPI.Models;
using CoolParking.WebAPI.Services;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/vehicles")]
    [ApiController]
    public class VehicleController : ControllerBase
    {
        private IParkingService parkingService;

        public VehicleController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        // GET: api/vehicles
        [HttpGet]
        public IActionResult GetVehicles()
        {
            var vehicles = parkingService.GetVehicles().ToList();
            var jsonString = JsonConvert.SerializeObject(vehicles);

            return Ok(jsonString);
        }

        //GET api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [Route("{id}")]
        [HttpGet]
        public IActionResult GetVehiclesById(string id)
        {
            if (!Validation.CheckId(id))
            {
                return StatusCode(400, "Invalid id");
            }

            var vehicle = parkingService.GetVehicles().FirstOrDefault(v => v.Id == id);
            if(vehicle == null)
            {
                return StatusCode(404, "Vehicle doesn't found");
            }

            var jsonString = JsonConvert.SerializeObject(VehicleVM.CreateInstance(vehicle));

            return Ok(jsonString);
        }

        //POST: api/vehicles
        [HttpPost]
        public IActionResult AddVehicle([FromBody]VehicleVM obj)
        {
            Vehicle vehicle;
            try
            {
                VehicleType vehicleType;
                switch (obj.VehicleType)
                {
                    case 0:
                        vehicleType = VehicleType.PassengerCar;
                        break;
                    case 1:
                        vehicleType = VehicleType.Truck;
                        break;
                    case 2:
                        vehicleType = VehicleType.Bus;
                        break;
                    case 3:
                        vehicleType = VehicleType.Motorcycle;
                        break;
                    default:
                        throw new ArgumentException("Invalid VehicleType");
                }
                vehicle = new Vehicle(obj.Id, vehicleType, obj.Balance);
                parkingService.AddVehicle(vehicle);
            }
            catch(Exception e)
            {
                return StatusCode(400, e.Message);
            }

            var jsonString = JsonConvert.SerializeObject(obj);

            return StatusCode(201, jsonString);
        }

        // DELETE api/vehicles/id (id - vehicle id of format “AA-0001-AA”)
        [Route("{id}")]
        [HttpDelete]
        public IActionResult RemoveVehicle(string id)
        {
            if (!Validation.CheckId(id))
            {
                return StatusCode(400, "Invalid id");
            }

            try
            {
                parkingService.RemoveVehicle(id);
            }
            catch(ArgumentException e)
            {
                return StatusCode(404, e.Message);
            }
            catch (InvalidOperationException e)
            {
                return StatusCode(400, e.Message);
            }

            return StatusCode(204, "Vehicle removed");
        }
    }
}
