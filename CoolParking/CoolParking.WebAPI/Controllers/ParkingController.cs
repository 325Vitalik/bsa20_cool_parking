﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using CoolParking.BL.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace CoolParking.WebAPI.Controllers
{
    [Route("api/parking")]
    [ApiController]
    public class ParkingController : ControllerBase
    {
        private IParkingService parkingService;

        public ParkingController(IParkingService parkingService)
        {
            this.parkingService = parkingService;
        }

        // GET: api/parking/balance
        [Route("balance")]
        [HttpGet]
        public ActionResult<decimal> GetBalance()
        {
            return parkingService.GetBalance();
        }

        // GET: api/parking/capacity
        [Route("capacity")]
        [HttpGet]
        public ActionResult<decimal> GetCapacity()
        {
            return parkingService.GetCapacity();
        }

        // GET api/parking/freePlaces
        [Route("freePlaces")]
        [HttpGet]
        public ActionResult<decimal> FreePlaces()
        {
            return parkingService.GetFreePlaces();
        }
    }
}
