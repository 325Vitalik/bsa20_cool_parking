﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class VehicleVM
    {
        [JsonProperty("id")]
        public string Id { get; set; } = "";
        [JsonProperty("vehicleType")]
        public int VehicleType { get; set; } = -1;
        [JsonProperty("balance")]
        public decimal Balance { get; set; } = -1;

        public static VehicleVM CreateInstance(CoolParking.BL.Models.Vehicle vehicle)
        {
            var newVehicle = new VehicleVM()
            {
                Id = vehicle.Id,
                VehicleType = (int)vehicle.VehicleType,
                Balance = vehicle.Balance
            };

            return newVehicle;
        }
    }
}
