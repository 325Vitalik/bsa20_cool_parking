﻿using CoolParking.BL.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class TransactionVM
    {
        [JsonProperty("transactionDate")]
        public DateTime Time { get; set; }
        [JsonProperty("vehicleId")]
        public string VehicleId { get; set; }
        [JsonProperty("sum")]
        public decimal Sum { get; set; }

        public static TransactionVM CreateInstance(TransactionInfo transaction)
        {
            var vm = new TransactionVM()
            {
                Time = transaction.Time,
                VehicleId = transaction.VehicleId,
                Sum = transaction.Sum
            };

            return vm;
        }
    }
}
