﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Models
{
    public class TopUpVM
    {
        [JsonProperty("id")]
        public string Id { get; set; } = "";

        [JsonProperty("Sum")]
        public decimal Sum { get; set; } = -1;
    }
}
