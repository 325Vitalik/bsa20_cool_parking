﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace CoolParking.WebAPI.Services
{
    public static class Validation
    {
        public static bool CheckId(string id)
        {

            var regexValidationId = new Regex(@"[A-Z]{2}-\d{4}-[A-Z]{2}");
            if (regexValidationId.IsMatch(id))
            {
                return true;
            }
            return false;
        }
    }
}
